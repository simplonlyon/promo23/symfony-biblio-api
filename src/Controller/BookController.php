<?php

namespace App\Controller;

use App\Entity\Book;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/book')]
class BookController extends AbstractController
{
 
    public function __construct(private BookRepository $repo){
 
    }

    #[Route(methods:'GET')]
    public function index(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(Book $book)
    {
        return $this->json($book);
    }


    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {

        try {
            $book = $serializer->deserialize($request->getContent(), Book::class, 'json');
            $this->repo->save($book,true);
            return $this->json($book, Response::HTTP_CREATED);
        } catch (\Throwable $th) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }


 /*    Exemple de code qui permet de changer la valeur de dispo de 1 à 0 */
    #[Route('/{id}/dispo',methods: 'POST')]
    public function dispo($id, SerializerInterface $serializer) {
        $book = $this->repo->find($id);
        $book->setDispo(0);
        $this->repo->save($book, true);
        return $this->json($book);
    }

}
